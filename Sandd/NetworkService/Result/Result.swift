//
//  Result.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//
import Foundation

enum Result<T, E: Error> {
    case success(T)
    case failure(E)
}
