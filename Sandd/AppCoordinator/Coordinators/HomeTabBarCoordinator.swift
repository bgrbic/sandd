//
//  HomeTabBarCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class HomeTabBarCoordinator : BaseCoordinator , HomeTabBarCoordinatorOutput{
    var logoutUser: (() -> Void)?
    
    var coordinatorFactory     : CoordinatorFactory
    var tabBarView             : HomeTabBarView
    
    init(tabbarView : HomeTabBarView,factory : CoordinatorFactory) {
        self.coordinatorFactory     = factory
        self.tabBarView             = tabbarView
    }
    override func start() {
        tabBarView.onPlanningSelectItem      = runPlanningFlow()
        tabBarView.onPerformanceSelectItem   = runPerformanceFlow()
        tabBarView.onChatSelectItem          = runChatFlow()
        tabBarView.onNotificationsSelectItem = runNotificationsFlow()
        tabBarView.onMoreSelectItem          = runMoreFlow()
        tabBarView.logoutUser = { [weak self] in
            self?.logoutUser?()
        }
    }
}
extension HomeTabBarCoordinator {
    private func runPerformanceFlow() -> ((UINavigationController) -> ()) {
        return { [weak self] navController in
            if navController.viewControllers.isEmpty == true {
                let performanceCoordinator = self?.coordinatorFactory.makeHomePerformanceCoordinator(navController: navController)
                performanceCoordinator?.start()
                self?.addDependency(performanceCoordinator!)
            }
        }
    }
    private func runChatFlow()-> ((UINavigationController)->()) {
        return { [weak self] navController in
            if navController.viewControllers.isEmpty == true {
                let chatCoordinator = self?.coordinatorFactory.makeHomeChatCoordinator(navController: navController)
                chatCoordinator?.start()
                self?.addDependency(chatCoordinator!)
            }
        }
    }
    private func runPlanningFlow()->((UINavigationController)->()) {
        return { [weak self] navController in
            if navController.viewControllers.isEmpty == true {
                let planningCoordinator = self?.coordinatorFactory.makeHomePlanningCoordinator(navController: navController)
                planningCoordinator?.start()
                self?.addDependency(planningCoordinator!)
            }
        }
    }
    private func runMoreFlow()->((UINavigationController)->()){
        return { [weak self] navController in
            if navController.viewControllers.isEmpty == true {
                let moreCoordinator = self?.coordinatorFactory.makeHomeMoreCoordinator(navController: navController)
                moreCoordinator?.start()
                self?.addDependency(moreCoordinator!)
            }
        }
    }
    private func runNotificationsFlow()->((UINavigationController)->()){
        return { [weak self] navController in
            if navController.viewControllers.isEmpty == true {
                let notificationsCoordinator = self?.coordinatorFactory.makeHomeNotificationsCoordinator(navController: navController)
                notificationsCoordinator?.start()
                self?.addDependency(notificationsCoordinator!)
            }
        }
    }
}
