//
//  ObserverCenter.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol Printable {
    var description : String {get}
}
private protocol Observable : class {
    associatedtype T
    
    var object: AnyObject? {get set}
    var f: (AnyObject) -> (T) -> Void {get}
    init(object: AnyObject, f: @escaping (AnyObject) -> (T) -> Void)
}

class ObserverEntry<Parameters> : Observable {
    typealias T = Parameters
    
    fileprivate weak var object: AnyObject?
    fileprivate var f: (AnyObject) -> (Parameters) -> Void
    
    required init(object: AnyObject, f: @escaping (AnyObject) -> (Parameters) -> Void) {
        self.object = object
        self.f = f
    }
}

class ObserverCenter<Parameters>: CustomStringConvertible {

    private var queue = DispatchQueue(label: "com.sandd.ObserverCenter", attributes: [])
    
    fileprivate func synchronized(_ f: () -> Void) {
        queue.sync(execute: f)
    }
    fileprivate var entries: [ObserverEntry<Parameters>] = []
    
    init() {}
    
    func add<T: AnyObject>(_ object: T, _ f: @escaping (T) -> (Parameters) -> Void) -> ObserverEntry<Parameters> {
        let entry = ObserverEntry<Parameters>(object: object, f: { f($0 as! T) })
        synchronized {
            self.entries.append(entry)
        }
        return entry
    }
    
    func add(_ f: @escaping (Parameters) -> Void) -> ObserverEntry<Parameters> {
        return self.add(self, { ignored in f })
    }
    
     func remove(_ entry: ObserverEntry<Parameters>) {
        synchronized {
            self.entries = self.entries.filter { $0 !== entry }
        }
    }
     func remove(_ object: AnyObject) {
        synchronized {
            self.entries = self.entries.filter { $0.object !== object }
        }
    }
     func notify(_ parameters: Parameters) {
        var toCall: [(Parameters) -> Void] = []
        
        synchronized {
            for entry in self.entries {
                if let object: AnyObject = entry.object {
                    toCall.append(entry.f(object))
                }
            }
            self.entries = self.entries.filter { $0.object != nil }
        }
        for function in toCall {
            function(parameters)
        }
    }
    deinit {
        entries.removeAll()
    }
}
extension ObserverCenter : Printable{
     var description: String {
        var entries: [ObserverEntry<Parameters>] = []
        synchronized {
            entries = self.entries
        }
        let strings = entries.map {
            entry in
            (entry.object === self
                ? "\(entry.f)"
                : "\(String(describing: entry.object)) \(entry.f)")
        }
        let joined = strings.joined(separator: ", ")
        
        return "\(Mirror(reflecting: self).description): (\(joined))"
    }
}
