//
//  ButtonsSignUpViewStyle.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/21/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//

import UIKit
class ButtonsSignUpViewStyle: UIButton {
    override func awakeFromNib() {
        super.awakeFromNib()
        switch self.titleLabel?.text {
        case "Login":
            self.backgroundColor    = UIColor(r: 230, g: 0, b: 110, alpha: 1)
        case "Registeren":
            self.backgroundColor    = UIColor(r: 152, g: 161, b: 178, alpha: 1)
        default:
            print("still not set")
        }
        layer.cornerRadius = 32
    }
    
    //MARK:- Animation on button click...
        override func touchesBegan(_ touches: Set<UITouch>, with event: UIEvent?) {
            transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.5, initialSpringVelocity: 6, options: .allowUserInteraction, animations: {
                self.transform = CGAffineTransform.identity
            }, completion: nil)
            super.touchesBegan(touches, with: event)
        }
}

