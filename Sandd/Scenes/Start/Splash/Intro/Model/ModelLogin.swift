//
//  ModelLogin.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

struct ModelLogin : Codable{
    /*
    private var username : String?
    private var password : String?
    */
    //MARK: Test "fake" json fields
     var  id       : UInt64!
     var  name     : String!
     var  email    : String!
     var  feedback : String!
     var  rating   : Int!
     var  source   : String!
    /*
     UserCredentials
    var email    : String?
    var password : String?
    var phone    : String?
    var adress   : String?
    var code     : String?
    */
    //end fake fields
    /*
    init(username : String = "",password : String = "") {
        self.username = username
        self.password = password
    }*/
    func login(_ completion: @escaping ((Result<ModelLogin, ErrorResult>) -> Void)){
        let worker = LoginWorker()
        worker.login(completion)
    }
}
