//
//  SplashViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/14/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol SplashView : BaseView {
    var onSuccessAuthorize : (()->Void)? {get set}
    var onFailedAuthorize  : (()->Void)? {get set}
}

class SplashViewController: UIViewController , SplashView{
    
    var onFailedAuthorize      : (() -> Void)?
    var onSuccessAuthorize     : (() -> Void)?

    private var viewModel      : SplashViewModelProtocol?
    let DELAY_ON_START_SECONDS : Double = 1.0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        viewModel = SplashViewModel()
        
        delay(seconds: DELAY_ON_START_SECONDS) {
            self.login()
        }
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
extension SplashViewController {
    private func login(){
            viewModel?.login{ (result) in
                //MARK - getLocalStore Authorization Data
                if let _ = UserCredentials().getUserCredentials() {
                    self.onSuccessAuthorize?()
                }else {
                     self.onFailedAuthorize?()
                }
                /*MARK -  THIS ENABLE AFTER FOR REAL REMOTE AUTHORIZATION
                switch result {
                case .success:
                     self.onSuccessAuthorize?()
                case .failure:
                     self.onFailedAuthorize?()
                }*/
         }
    }
}
