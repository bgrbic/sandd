//
//  HomePlanningCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol HomeCoordinatorOutput : class {
    var finishFlow: (() -> Void)? { get set }
}
protocol HomePlanningModulFactory {
      func makeHomePlanningOutput()->HomePlanningView
}
class HomePlanningCoordinator : BaseCoordinator , HomeCoordinatorOutput{
    
    var finishFlow: (() -> Void)?
    
    private var coordinatorFactory  : CoordinatorFactory
    private var router              : Router
    private var factory             : HomeModuleFactory
    
    init(router : Router,factory : HomeModuleFactory,coordinatorFactory : CoordinatorFactory) {
        self.router             = router
        self.coordinatorFactory = coordinatorFactory
        self.factory            = factory
    }
    override func start() {
        print("HomePlanningCoordinator")
    }
}
