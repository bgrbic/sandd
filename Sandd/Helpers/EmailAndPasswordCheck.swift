//
//  EmailAndPasswordCheck.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/22/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//

import Foundation
struct EmailAndPasswordCheck  {
    //MARK - Validate email.
    func validateEmail (email: String )->Bool {
        let REGEX: String
        REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,32}"
        return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: email)
    }
    //MARK - password at least 6 character ...
    func validatePasswordFormat(_ password: String) -> Bool
    {
        let trimmedString = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
        return trimmedString.count > 5
    }
    //MARK:- Check password and email
    func passwordAndEmailCheck(email:String?, password:String?)->Bool {
        var validEmail      :Bool!
        var validPassword   :Bool!
        
        if email == nil || password == nil {
            return false
        }
        validEmail = validateEmailFormat(email!)
        validPassword = validatePasswordFormat(password!)
        if validEmail && validPassword {
            return true
        }
     return false
    }
    func checkPasswordRepeatPassword(password:String?,repeatedPassword:String?)->(Bool,String?){
        if password == "",repeatedPassword == "" {
           return (false,"Please fill in all fields")
        }
        
        if password != repeatedPassword {
            return (false,"Passwords do not match")
        }
        
        if self.validatePasswordFormat(password!) == false {
            return (false,"Pleaese input longer password")
        }
        
        return (true,nil)
    }
}
