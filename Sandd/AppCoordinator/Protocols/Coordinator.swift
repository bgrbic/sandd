//
//  Coordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/17/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol Coordinator : class {
    func start()
    func start(with option: LauncerManagerOption?)
}
