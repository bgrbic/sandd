//
//  HomeMoreViewDataSource.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class HomeMoreViewDataSource : NSObject , UITableViewDataSource {
    
    private var tableView       : UITableView?
    private var arrayOfButtons  : [ModelMore] = []
    private var model           : MoreViewModelProtocol!
    var didSelectLogout         : (()->Void)?
    
    init(tableView : UITableView, model : MoreViewModelProtocol){
        super.init()
        self.tableView = tableView
        self.model     = model
        setupViews()
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return arrayOfButtons.count
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        let defaultCellHeight = (tableView.frame.height-47)/6
        
        return indexPath.row == arrayOfButtons.count - 1 ? defaultCellHeight + 32 : defaultCellHeight
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let element  = arrayOfButtons[indexPath.row]
        
        if indexPath.row == arrayOfButtons.count - 1 {
            let cell = tableView.dequeueReusableCell(CellMoreLogout.self, at: indexPath)
            cell.model = element
            cell.hideSeparator()
            return cell
        }else {
            let cell = tableView.dequeueReusableCell(CellMoreDefault.self, at: indexPath)
            cell.model = element
            if checkNeedForCellSeparator(indexRow: indexPath.row) {
                cell.hideSeparator()
            }
            return cell
        }
    }
    
    func checkNeedForCellSeparator(indexRow:Int)->Bool{
        if indexRow == arrayOfButtons.count - 1 || indexRow == arrayOfButtons.count - 2 {
            return true
        }
        return false
    }
    
}
extension HomeMoreViewDataSource : UITableViewDelegate {
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        
        if indexPath.row == arrayOfButtons.count - 1 {
            didSelectLogout?()
        }
    }
}
extension HomeMoreViewDataSource : UIViewStyling {
    func setupViews() {
        tableView?.tableFooterView = UIView()
        model?.getElements(result: { (elements) in
            arrayOfButtons = elements
        })
    }
}
