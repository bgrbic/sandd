//
//  LoginNetworking.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol LoginNetworkProtocol {
    func login(_ completion: @escaping ((Result<ModelLogin,ErrorResult>)->Void))
}
class LoginNetworking : RequestHandler {
    private let urlString = ServerAPI.shared.loginAuthorization
    private var task      : URLSessionTask?
}
extension LoginNetworking : LoginNetworkProtocol {
    func login(_ completion: @escaping ((Result<ModelLogin, ErrorResult>) -> Void)) {
        
        if let _task = task {
            _task.cancel()
        }
        task = RequestService().loadData(urlString: urlString, completion: self.networkResult(completion: completion))
    }
}
struct LoginWorker {
  
    let loginNetworker = LoginNetworking()
    func login(_ completion: @escaping ((Result<ModelLogin, ErrorResult>) -> Void)) {
        loginNetworker.login(completion)
    }
}
