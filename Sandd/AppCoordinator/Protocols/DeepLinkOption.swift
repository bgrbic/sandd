//
//  DeepLinkOption.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

struct DeepLinkURLConstants {
    static let Start         = "start"
    static let Home          = "home"
    static let Chat          = "chat"
    static let Planning      = "planning"
    static let Performance   = "performance"
    static let Notifications = "notifications"
    static let More          = "more"
    
}
enum LauncerManagerOption {
    case start ,home ,chat ,planning ,performance ,notifications ,more

static func build(with userActivity: NSUserActivity) -> LauncerManagerOption? {
    if userActivity.activityType == NSUserActivityTypeBrowsingWeb,
        let url = userActivity.webpageURL,
        let _ = URLComponents(url: url, resolvingAgainstBaseURL: true) {
        //TODO: extract string and match with DeepLinkURLConstants to jump
    }
    return nil
}
static func build(with dict: [String : AnyObject]?) -> LauncerManagerOption? {
    guard let id = dict?["launch_id"] as? String else { return nil }
    
    switch id {
    case DeepLinkURLConstants.Start:         return .start
    case DeepLinkURLConstants.Chat:          return .chat
    case DeepLinkURLConstants.Home:          return .home
    case DeepLinkURLConstants.More:          return .more
    case DeepLinkURLConstants.Notifications: return .notifications
    case DeepLinkURLConstants.Performance:   return .performance
    case DeepLinkURLConstants.Planning:      return .planning
    default: return nil
    }
  }
}
