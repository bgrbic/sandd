//
//  IntroVideoViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol IntroVideoView : BaseView {
    var onSwipe : ((_ currentVideoTime : Float64?)->Void)? {get set}
    var currentMediaTimePlayback : Float64? {get set}
}
class IntroVideoViewController: UIViewController , IntroVideoView{
    var currentMediaTimePlayback: Float64?
    var onSwipe: ((_ currentVideoTime : Float64?) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func userDidSwipe(_ sender: Any) {
        var currentVideoTime : Float64? = nil
        let _subviews = view.subviews.filter{ $0 is VideoIntroView }
        if let _videoIntro = _subviews.first as? VideoIntroView {
              _videoIntro.videoPLayer?.stopPlaying()
            currentVideoTime = _videoIntro.videoPLayer?.currentMediaTimePlayback
        }
        onSwipe?(currentVideoTime)
    }
}
extension IntroVideoViewController {
    override func viewWillAppear(_ animated: Bool) {
        if let videoIntroView  = view as? VideoIntroView {
            videoIntroView.videoPLayer?.startPlaying()
        }
    }
    override func viewWillDisappear(_ animated: Bool) {
        if let videoIntroView  = view as? VideoIntroView {
            videoIntroView.videoPLayer?.stopPlaying()
            videoIntroView.removeFromSuperview()
        }
    }
}
