//
//  SplashViewModel.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol SplashViewModelProtocol {
    func login(_ completion: @escaping ((Result<ModelLogin, ErrorResult>) -> Void))
}
struct SplashViewModel : SplashViewModelProtocol{
    
    private let model  = ModelLogin()
   
    func login(_ completion: @escaping ((Result<ModelLogin, ErrorResult>) -> Void)) {
        model.login(completion)
    }
}
