//
//  AppDelegate+Exceptions.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

extension AppDelegate {
    
    private func handleOfflineStatus(isReachable : Bool){
        guard let navController = window?.rootViewController as? UINavigationController else { return }
        switch isReachable {
        case false:
            let offlineController = NetworkOfflineViewController.controllerFromStoryboard(.start)
            navController.present(offlineController, animated: true, completion: nil)
        default:
            guard navController.presentedViewController is NetworkOfflineViewController else { return }
            navController.dismiss(animated: true, completion: nil)
        }
    }
    private func handleLocationOffStatus(isLocationOff: Bool){
        guard let navController = window?.rootViewController as? UINavigationController else { return }
        
        switch isLocationOff {
        case true:
            let locationController = LocationOffViewController.controllerFromStoryboard(.start)
            delay(seconds: 1.5) {
                navController.present(locationController, animated: true, completion: nil)
            }
        default:
            guard navController.presentedViewController is LocationOffViewController else { return }
            navController.dismiss(animated: true, completion: nil)
        }
    }
}
extension AppDelegate {
     func setupReachability(){
        networkReachable =  Reachability()!
        networkReachable.whenReachable = { [weak self] _  in
            mainThread {
                self?.handleOfflineStatus(isReachable: true)
            }
        }
        networkReachable.whenUnreachable = { [weak self] _ in
            mainThread {
                self?.handleOfflineStatus(isReachable: false)
            }
        }
        do {
            try networkReachable.startNotifier()
        } catch {
            debugPrint("Unable to start reachability")
        }
    }
     func setupLocationTracking(){
        locationManager.whenLocationDisable = { [weak self] in
            self?.handleLocationOffStatus(isLocationOff: true)
        }
        locationManager.whenLocationEnable = { [weak self] in
            self?.handleLocationOffStatus(isLocationOff: false)
        }
        locationManager.checkStatus()
    }
}
