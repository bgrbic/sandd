//
//  CellMoreLogout.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/23/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class CellMoreLogout: UITableViewCell {

    static let identifier = "CellMoreLogout"
    @IBOutlet weak var imageMoreElement: UIImageView!
    @IBOutlet weak var labelMoreTitle: UILabel!
    
    var model : ModelMore? {
        didSet {
            guard let model = model else {
                return
            }
            if let _title = model.title {
                labelMoreTitle.text = _title
            }
            if let _image = model.image {
                imageMoreElement.image = UIImage(named: _image)
            }
        }
    }
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
}
