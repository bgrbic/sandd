//
//  UIColor+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/2/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

extension UIColor {
    static func colorFromHex(_ rgbValue: UInt32, alpha: CGFloat) -> UIColor {
        let red   = CGFloat((rgbValue & 0xFF0000) >> 16)/256.0
        let green = CGFloat((rgbValue & 0xFF00) >> 8)/256.0
        let blue  = CGFloat(rgbValue  & 0xFF)/256.0
        return UIColor(red: red, green: green, blue: blue, alpha: alpha)
    }
    convenience init(r: CGFloat, g: CGFloat, b: CGFloat,alpha:CGFloat) {
        self.init(red: r/255, green: g/255, blue: b/255, alpha:alpha)
    }
}
