//
//  UINib+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/17/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

private extension UINib {
    static func nib(named nibName: String) -> UINib {
        return UINib(nibName: nibName, bundle: nil)
    }
    static func loadSingleView(_ nibName: String, owner: Any?) -> UIView {
        return nib(named: nibName).instantiate(withOwner: owner, options: nil).first as! UIView
    }
}
extension UINib {
    class func loadVideoStartView(_ owner: AnyObject) -> UIView {
        return loadSingleView("VideoStartView", owner: owner)
    }
}
