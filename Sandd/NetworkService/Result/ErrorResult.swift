//
//  ErrorResult.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/13/18.
//  Copyright © 2018 Milos Stevanovic. All rights reserved.
//
import Foundation

enum ErrorResult: Error {
    case network(string: String)
    case parser(string: String)
    case custom(string: String)
}

