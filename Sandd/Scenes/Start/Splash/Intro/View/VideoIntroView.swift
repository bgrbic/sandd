//
//  VideoIntroView.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation
import UIKit

class VideoIntroView: UIView {
    
    private lazy var imageLayer : ImageLayer?  = {
        guard let _imageLayer = ImageLayer(superView: self, image: #imageLiteral(resourceName: "background").cgImage,opacity:0.5)else{return nil }
        return _imageLayer
    }()
    lazy var videoPLayer  : VideoPlayer? =  {
        let videoURL = Bundle.main.url(forResource: "video_intro", withExtension: "m4v")
        guard let _videoPLayer = VideoPlayer(superView: self, videoURL: videoURL) else {  return  nil  }
        return _videoPLayer
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setupViews()
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setupViews()
    }
}
extension VideoIntroView : UIViewStyling {
       func setupViews() {
      
        guard let _imageLayer = imageLayer else {
            debugPrint("ImageLayer can't load ! \(#function)")
            return
        }
        guard let _videoPlayer = videoPLayer ,let _videoLayer = _videoPlayer.videoLayer else {
            debugPrint("Video player can't load ! \(#function)")
            return
        }
        layer.addSublayer(_videoLayer)
        _videoPlayer.startPlaying()
        layer.addSublayer(_imageLayer)
    }
    func removeViews(){
        for subview in self.subviews {
            subview.removeFromSuperview()
        }
    }
}
