//
//  UIViewController+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

extension UIViewController {
    
    func insertChildController(_ childController: UIViewController, intoParentView parentView: UIView) {
        childController.willMove(toParentViewController: self)
        
        self.addChildViewController(childController)
        childController.view.frame = parentView.bounds
        parentView.addSubview(childController.view)
    
        childController.didMove(toParentViewController: self)
    }
}
