//
//  ImageLayer.swift
//  Sandd
//
//  Created by Branko Grbic on 8/1/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit
import Foundation

class ImageLayer : CALayer {
    
    init?(superView : UIView,image : CGImage?,opacity: Float = 1.0){
        super.init()
    
        guard let imageCg = image else { print("ImageDoesn'tExist! \(#function)")
            return nil }
        contents        = imageCg
        frame           = superView.bounds
        contentsScale   = UIScreen.main.scale
        shouldRasterize = true
        self.opacity    = opacity
    }
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
