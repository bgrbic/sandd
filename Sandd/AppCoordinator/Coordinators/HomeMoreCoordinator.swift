//
//  HomeMoreCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol HomeMoreCoordinatorOutput : class {
    var finishFlow: (() -> Void)? { get set }
}
protocol HomeMoreModulFactory {
    func makeHomeMoreOutput()->HomeMoreView
}
class HomeMoreCoordinator : BaseCoordinator , HomeMoreCoordinatorOutput{
    var finishFlow: (() -> Void)?
    
    private var factory : HomeModuleFactory
    private var coordinatorFactory : CoordinatorFactory
    private var router : Router
    
    init(router : Router,factory:HomeModuleFactory,coordinatorFactory: CoordinatorFactory) {
        self.router  = router
        self.factory = factory
        self.coordinatorFactory = coordinatorFactory
    }
    override func start() {
        print("HomeMoreCoordinator")
    }
}
extension HomeMoreCoordinator {
    
}
