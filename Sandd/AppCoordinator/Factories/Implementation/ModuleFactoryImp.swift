//
//  ModuleFactoryImp.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

final class ModuleFactoryImp : StartModuleFactory , HomeModuleFactory {
    
    func makeIntroStartTourOutput() -> IntroStartTourView {
        return IntroStartTourViewController.controllerFromStoryboard(.start)
    }
    func makeFindeVacanciesOuput() -> FindeVacanciesView {
        return FindeVacanciesViewController.controllerFromStoryboard(.start)
    }
    func makeLoginOutput() -> LoginView {
        return LoginViewController.controllerFromStoryboard(.start)
    }
    func makeNewPasswordOutput() -> NewPasswordView {
        return NewPasswordViewController.controllerFromStoryboard(.start)
    }
    func makeForgotPasswordOutput() -> ForgotPasswordView {
        return ForgotPasswordViewController.controllerFromStoryboard(.start)
    }
    func makeRegisterOutput() -> RegisterView {
        return RegisterViewController.controllerFromStoryboard(.start)
    }
    func makeRegisterSetPasswordOutput() -> RegisterSetPasswordView {
        return RegisterSetPasswordViewController.controllerFromStoryboard(.start)
    }
    func makeRegisterPersonalDataOutput() -> RegisterPersonalDataView {
        return RegisterPersonalDataViewController.controllerFromStoryboard(.start)
    }
    func makeRegisterStartTourOutput() -> RegisterStartTourView {
        return RegisterStartTourViewController.controllerFromStoryboard(.start)
    }
    func makeHomeTabBarOutput() -> HomeTabBarView {
       return HomeTabBarController.controllerFromStoryboard(.home)
    }
    func makeHomePerformanceOutput() -> HomePerformanceView {
        return HomePerformanceViewController.controllerFromStoryboard(.home)
    }
    func makeHomeChatOutput() -> HomeChatView {
        return HomeChatViewController.controllerFromStoryboard(.home)
    }
    func makeHomePlanningOutput() -> HomePlanningView {
        return HomePlanningViewController.controllerFromStoryboard(.home)
    }
    func makeHomeNotificationOutput() -> HomeNotificationsView {
        return HomeNotificationsViewController.controllerFromStoryboard(.home)
    }
    func makeHomeMoreOutput() -> HomeMoreView {
        return HomeMoreViewController.controllerFromStoryboard(.home)
    }
    func makeSplashOutput() -> SplashView {
       return SplashViewController.controllerFromStoryboard(.start)
    }
    func makeIntroVideoOutput() -> IntroVideoView {
       return IntroVideoViewController.controllerFromStoryboard(.start)
    }
    func makeSignUpOutput(currentVideoTime : Float64?) -> SignUpView {
       let signUpComtroller =  SignUpViewController.controllerFromStoryboard(.start)
        signUpComtroller.currentMediaTimePlayback = currentVideoTime
        return signUpComtroller
    }
}
