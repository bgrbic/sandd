//
//  SignUpViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol SignUpView : BaseView {
    var onLoginClick            : (()->Void)? {get set}
    var onRegisterClick         : (()->Void)? {get set}
    var onFindeVacancies        : (()->Void)? {get set}
}
class SignUpViewController: UIViewController , SignUpView{
    
    var onLoginClick        : (() -> Void)?
    var onRegisterClick     : (() -> Void)?
    var onFindeVacancies    : (() -> Void)?
    var currentMediaTimePlayback : Float64?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        continueVideoPlay()
    }
    private func continueVideoPlay(){
         let _subviews = view.subviews.filter{ $0 is VideoIntroView }
         if let _videoIntro = _subviews.first as? VideoIntroView {
           //MARK - uncoment this for continue playing video on this screen
          //if they don't use video , just remove VideoIntro from subview
         //_videoIntro.videoPLayer?.currentMediaTimePlayback = currentMediaTimePlayback
            _videoIntro.videoPLayer?.removePlayer()
       }
    }
    @IBAction func didPressLogin(_ sender: Any) {
        getVideoPlayer()?.stopPlaying()
        onLoginClick?()
    }
    @IBAction func didPressRegister(_ sender: Any) {
        getVideoPlayer()?.stopPlaying()
        onRegisterClick?()
    }
    @IBAction func didPressVacancies(_ sender: Any){
        getVideoPlayer()?.stopPlaying()
        onFindeVacancies?()
    }
}
extension SignUpViewController {
    private func getVideoPlayer()->VideoPlayer?{
        if let _videoIntro = view.subviews.first as? VideoIntroView {
            return _videoIntro.videoPLayer
        }
        return nil
    }
    override func viewWillAppear(_ animated: Bool) {
        getVideoPlayer()?.startPlaying()
    }
    override func viewWillDisappear(_ animated: Bool) {
        getVideoPlayer()?.stopPlaying()
    }
}
