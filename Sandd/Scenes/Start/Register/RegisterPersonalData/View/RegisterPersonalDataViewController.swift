//
//  RegisterPersonalDataViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol RegisterPersonalDataView : BaseView {
    var onPressBack : (()->Void)? {get set}
    var onPressStartTour : (()->Void)? {get set}
}
class RegisterPersonalDataViewController: BaseViewController , RegisterPersonalDataView{
    
    @IBOutlet weak var btnContinue          : UIButton!
    @IBOutlet weak var textFieldName        : UITextField!
    @IBOutlet weak var textFieldLastName    : UITextField!
    @IBOutlet weak var textFieldTelephone   : UITextField!
    
    var onPressStartTour:   (() -> Void)?
    var onPressBack:        (() -> Void)?
    private var orginPoint  : CGPoint?
    
    @IBOutlet weak var txtphoneNumber: UITextField! {
        didSet {
            txtphoneNumber?.addDoneCancelToolbar()
        }
    }
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func didPressStartTour(_ sender: UIButton) {
        updateUserCredentials()
        onPressStartTour?()
    }
    private func updateUserCredentials(){
        if var user = UserCredentials().getUserCredentials(){
            user.name     = textFieldName.text
            user.lastName = textFieldLastName.text
            user.phone    = textFieldTelephone.text
            user.saveUserCredentials()
        }
    }
    @IBAction func didPressBack(_ sender: UIBarButtonItem) {
        onPressBack?()
    }
}
extension RegisterPersonalDataViewController {
    func setupKeyboard(){
        customizeKeyboard(button: btnContinue, textFields: [textFieldName,textFieldLastName,textFieldTelephone])
        continueButton.addTarget(self, action: #selector(didPressStartTour(_:)), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupKeyboard()
    }
}
