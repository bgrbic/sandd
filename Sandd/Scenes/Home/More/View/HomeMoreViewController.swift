//
//  HomeMoreViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol HomeMoreView : BaseView{
    var didSelectLogout : (()->Void)? {get set}
}
class HomeMoreViewController: BaseViewController , HomeMoreView{
    
    var didSelectLogout: (() -> Void)?
   
    @IBOutlet weak var table    : UITableView!
    private var model           : MoreViewModelProtocol!
    private var arrayOfButtons  : [ModelMore] = []
    private var dataSource      : HomeMoreViewDataSource?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
        dataSource?.didSelectLogout = { [weak self] in
            self?.didSelectLogout?()
        }
    }
}
extension HomeMoreViewController : UIViewStyling {
    func setupViews() {
        model = MoreViewModel()
        dataSource = HomeMoreViewDataSource(tableView: table, model: model)
        table.delegate   = dataSource
        table.dataSource = dataSource
    }
}
