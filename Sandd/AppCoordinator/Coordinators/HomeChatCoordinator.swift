//
//  HomeChatCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol HomeChatModulFactory {
     func makeHomeChatOutput()->HomeChatView
}
class HomeChatCoordinator : BaseCoordinator {
    
    private var router             : Router
    private var factory            : HomeModuleFactory
    private var coordinatorFactory : CoordinatorFactory
    
    init(router : Router,factory : HomeModuleFactory,coordinatorFactory : CoordinatorFactory){
       self.factory = factory
       self.router  = router
       self.coordinatorFactory = coordinatorFactory
    }
    override func start() {
        print("HomeChatCoordinator")
    }
}
extension HomeChatCoordinator {
    
}
