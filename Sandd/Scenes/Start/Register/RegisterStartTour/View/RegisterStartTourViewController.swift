//
//  RegisterStartTourViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol RegisterStartTourView : BaseView {
    var onPressShowIntro : (()->Void)? {get set}
}
class RegisterStartTourViewController: UIViewController , RegisterStartTourView{
    
    var onPressShowIntro: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        print("RegisterStartTourViewController")
    }
    @IBAction func didPressShowIntro(_ sender: Any){
        onPressShowIntro?()
    }
}
