//
//  BaseView.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol BaseView: NSObjectProtocol, Presentable { }
