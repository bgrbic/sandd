//
//  LoginViewModel.swift
//  Sandd
//
//  Created by Branko Grbic on 8/17/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol LoginViewModelProtocol {
    func getUserCredentials( user:(UserCredentials?)->Void)
    var  setUserCredentials : ((UserCredentials)->Void)?  {get set}
}
struct LoginViewModel : LoginViewModelProtocol {
  
    var setUserCredentials: ((UserCredentials) -> Void)?
    private var model : ModelLoginViewController?
    
    init(){
        model = ModelLoginViewController()
        let mySelf = self
        setUserCredentials  = { (user) in
            mySelf.model?.saveUser(user: user)
        }
    }
    func getUserCredentials(user: (UserCredentials?) -> Void) {
        user(model?.getUser())
    }
    
}
