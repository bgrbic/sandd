//
//  UIFont+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/2/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

public enum FontStyleOpenSans: String {
    static let fontFamilyName = "HelveticaNeue"
    case light          = "Light"
    case regular        = "Regular"
    case medium         = "Medium"
    case bold           = "Bold"
    
    var name: String {
        switch self {
        case .regular:
            return FontStyleOpenSans.fontFamilyName
        case .light, .medium, .bold:
            return "\(FontStyleOpenSans.fontFamilyName)-\(rawValue)"
        }
    }
}
public enum FontStyleProximaNova: String {
    static let fontFamilyName = "Proxima-Nova-Soft"
    case bold               = "Bold"
    case regular            = "Regular"
    case medium             = "Medium"
    case semibold           = "Semibold"
    
    var name: String {
        switch self {
        case .regular:
            return FontStyleProximaNova.fontFamilyName
        case .bold, .semibold, .medium:
            return "\(FontStyleProximaNova.fontFamilyName)-\(rawValue)"
        }
    }
}


extension UIFont {
    static func proximaNova(type: FontStyleOpenSans, size: CGFloat) -> UIFont {
        return UIFont(name: type.name, size: size) ?? .systemFont(ofSize: size)
    }
    static func openSans(type: FontStyleProximaNova,size:CGFloat) -> UIFont {
        return UIFont(name: type.name, size: size) ?? .systemFont(ofSize: size)
    }
}
