//
//  Presentable.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol Presentable {
    func toPresent() -> UIViewController?
}
extension UIViewController: Presentable {
    
    func toPresent() -> UIViewController? {
        return self
    }
}
