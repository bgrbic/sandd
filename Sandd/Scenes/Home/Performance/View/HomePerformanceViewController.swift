//
//  HomePerformanceViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol HomePerformanceView : BaseView{
    
}
class HomePerformanceViewController: UIViewController , HomePerformanceView{

    override func viewDidLoad() {
        super.viewDidLoad()
        print("HomePerformanceViewController")
    }
}
