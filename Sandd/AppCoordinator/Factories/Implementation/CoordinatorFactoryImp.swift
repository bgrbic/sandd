//
//  CoordinatorFactoryImp.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

final class CoordinatorFactoryImp: CoordinatorFactory {

    func makeHomeChatCoordinator(navController: UINavigationController?) -> Coordinator {
        let chatCoordinator = HomeChatCoordinator(router: router(navController), factory: ModuleFactoryImp(), coordinatorFactory: CoordinatorFactoryImp())
        return chatCoordinator
    }
    func makeHomeChatCoordinator() -> Coordinator {
        return makeHomeChatCoordinator(navController: nil)
    }
    func makeHomePlanningCoordinator(navController: UINavigationController?) -> Coordinator & HomeCoordinatorOutput{
        let planningCoordinator = HomePlanningCoordinator(router: router(nil), factory: ModuleFactoryImp(), coordinatorFactory: CoordinatorFactoryImp())
        return planningCoordinator
    }
    func makeHomePlanningCoordinator() -> Coordinator & HomeCoordinatorOutput {
        return makeHomePlanningCoordinator(navController: nil)
    }
    func makeHomeMoreCoordinator(navController: UINavigationController?) -> Coordinator {
        let moreCoordinator = HomeMoreCoordinator(router: router(navController), factory: ModuleFactoryImp(), coordinatorFactory: CoordinatorFactoryImp())
        return moreCoordinator
    }
    func makeHomeMoreCoordinator() -> Coordinator {
        return makeHomeMoreCoordinator(navController: nil)
    }
    func makeHomeNotificationsCoordinator(navController: UINavigationController?) -> Coordinator {
        let notificationCoordinator = HomeNotificationsCoordinator(router: router(navController), factory: ModuleFactoryImp(), coordinatorFactory: CoordinatorFactoryImp())
        return notificationCoordinator
    }
    func makeHomeNotificationsCoordinator() -> Coordinator {
        return makeHomeNotificationsCoordinator(navController: nil)
    }
    func makeHomePerformanceCoordinator(navController: UINavigationController?) -> Coordinator {
        let performanceCoordinator = HomePerformanceCoordinator(router: router(navController), factory: ModuleFactoryImp(), coordinatorFactory: CoordinatorFactoryImp())
        return performanceCoordinator
    }
    func makeHomePerformanceCoordinator() -> Coordinator {
        return makeHomePerformanceCoordinator(navController: nil)
    }
    func makeHomeTabBarCoordinator() -> (configurator: Coordinator & HomeTabBarCoordinatorOutput, toPresent: Presentable?) {
    let tabController = HomeTabBarController.controllerFromStoryboard(.home)
    let tabCoordinator = HomeTabBarCoordinator(tabbarView: tabController, factory: CoordinatorFactoryImp())
        return (tabCoordinator,tabController)
    }
    func makeStartCoordinator(router : Router ,startWithScrenn : StartOutputScreen)->Coordinator & StartCoordinatorOutput {
        let coordinator = StartCoordinator(router: router, factory: ModuleFactoryImp(), startWithScreen: startWithScrenn)
        return coordinator
    }
    private func navigationController(_ navController: UINavigationController?) -> UINavigationController {
        if let navController = navController { return navController }
        else { return UINavigationController.controllerFromStoryboard(.start) }
    }
    private func router(_ navController: UINavigationController?) -> Router {
        return RouterImp(rootController: navigationController(navController))
    }
}
