//
//  Helpers.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

func mainThread(_ block:@escaping()->Void){
    DispatchQueue.main.async {
        block()
    }
}
func delay(seconds : Double , task:@escaping()->Void){
    DispatchQueue.main.asyncAfter(deadline: .now() + seconds, execute: task)
}
func measureBlock(_ block: () -> Void){
    let start   = CFAbsoluteTimeGetCurrent()
    block()
    let end     = CFAbsoluteTimeGetCurrent()
    print(String(format: "measure execution time in seconds: %.5f", end - start))
}
func validateEmailFormat(_ email: String) -> Bool
{
    let REGEX: String
    REGEX = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,32}"
    return NSPredicate(format: "SELF MATCHES %@", REGEX).evaluate(with: email)
}
func validatePasswordFormat(_ password: String) -> Bool
{
    //MARK - password at least 6 character ...
    let trimmedString = password.trimmingCharacters(in: CharacterSet.whitespacesAndNewlines)
    return trimmedString.count > 5
}
