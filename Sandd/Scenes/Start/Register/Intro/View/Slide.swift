//
//  Slide.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class Slide: UIView {
    @IBOutlet weak var buttonStart: UIButton!
    @IBOutlet weak var imageView    : UIImageView!
    @IBOutlet weak var title        : UILabel!
    @IBOutlet weak var subtitle     : UILabel!
    @IBOutlet weak var btnOverslaan : UIButton!
    private var _index : Int = 0
    
    var index : Int {
        get { return _index }
        set {
            _index = newValue
            btnOverslaan.isHidden = newValue == 0 ? false : true
        }
    }
}
