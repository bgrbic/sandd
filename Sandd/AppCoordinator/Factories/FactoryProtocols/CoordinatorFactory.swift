//
//  CoordinatorFactory.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol CoordinatorFactory {
    
    func makeStartCoordinator(router : Router,startWithScrenn : StartOutputScreen)->Coordinator & StartCoordinatorOutput
    
    func makeHomeTabBarCoordinator() -> (configurator: Coordinator & HomeTabBarCoordinatorOutput, toPresent: Presentable?)
    
    func makeHomePerformanceCoordinator() -> Coordinator
    func makeHomePerformanceCoordinator(navController: UINavigationController?) -> Coordinator
    
    func makeHomeChatCoordinator()->Coordinator
    func makeHomeChatCoordinator(navController: UINavigationController?)->Coordinator
    
    func makeHomePlanningCoordinator()->Coordinator & HomeCoordinatorOutput
    func makeHomePlanningCoordinator(navController:UINavigationController?)->Coordinator & HomeCoordinatorOutput
    
    func makeHomeMoreCoordinator()->Coordinator
    func makeHomeMoreCoordinator(navController:UINavigationController?)->Coordinator
    
    func makeHomeNotificationsCoordinator()->Coordinator
    func makeHomeNotificationsCoordinator(navController:UINavigationController?)->Coordinator
}
