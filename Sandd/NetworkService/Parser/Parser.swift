//
//  Parser.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

final class ParserHelper{
    static func parse<T: Decodable>(data: Data, completion : @escaping (Result<[T], ErrorResult>) -> Void){
        
        let jsonDecoder = JSONDecoder()
        
        do {
            let models = try jsonDecoder.decode([T].self, from: data)
            mainThread {
                completion(.success(models))
            }
        } catch {
            mainThread {
                completion(.failure(.parser(string: "Error while parsing json array data \(T.self)")))
            }
       }
    }
    static func parse<T : Decodable>(data: Data, completion : @escaping (Result<T, ErrorResult>) -> Void) {
        
            let jsonDecoder = JSONDecoder()
            do {
                let model = try jsonDecoder.decode(T.self, from: data)
                mainThread {
                    completion(.success(model))
                }
            } catch {
                mainThread {
                    completion(.failure(.parser(string: "Error while parsing json data \(T.self)")))
                }
            }
    }
}

