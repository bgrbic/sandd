//
//  TourViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol IntroStartTourView : BaseView {
    var showHome : (()->Void)? {get set}
}
class IntroStartTourViewController: UIViewController , IntroStartTourView{
    
    var showHome: (() -> Void)?
    
    @IBOutlet weak var scrollView   : UIScrollView!
    @IBOutlet weak var pageControl  : UIPageControl!
    
    private var slides:[Slide] = []
    private let images         = [#imageLiteral(resourceName: "tourScreenOne"),#imageLiteral(resourceName: "tourScreenTwo"),#imageLiteral(resourceName: "tourScreenTree"),#imageLiteral(resourceName: "tourScreenFour"),#imageLiteral(resourceName: "tourScreenFive"),#imageLiteral(resourceName: "tourScreenSix"),#imageLiteral(resourceName: "tourScreenSeven"),#imageLiteral(resourceName: "tourScreenEight")]
    private let titles         = [ "Planningsoverzicht","Optimale bezorgroute","Deel jouw tips",
                                   "Coach","Meer verdienen","Blijf in contact","Meer beloningen","Meer beloningen"     ]
    private let subtitles = ["Één duidelijk overzicht met jouw taken voor de eerstvolgende bezorgdag(en)", "De app geeft automatisch een suggestie voor jouw meest optimale bezorgroute","Deel jouw ervaringen en tips met de rest van de Sandd bezorgers", "Krijg feedback van de in-app coach en verbeter jouw prestaties als Sandd bezorger","Verdien extra bij door het uitvoeren van extra taken tijdens het bezorgen van de post","Een vraag? Of feedback? Start een chat met jouw Sandd coördinator.","Verzamel punten tijdens het bezorgen van de post en ontgrendel mooie beloningen","Verzamel punten tijdens het bezorgen van de post en ontgrendel mooie beloningen"]
    
     @IBAction func actionOpenHome() {
        showHome?()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = true
    }
}
extension IntroStartTourViewController : UIScrollViewDelegate {
    func scrollViewDidScroll(_ scrollView: UIScrollView){
        let currentIndex    = Int(round(scrollView.contentOffset.x/view.frame.width))
        pageControl.currentPage = currentIndex
        let currentSlide = slides[currentIndex]
        
        UIView.animate(withDuration: 0.2) {
            currentSlide.imageView.transform = CGAffineTransform(scaleX: 1.1, y: 1.1)
            if currentIndex - 1 >= 0 {
                let previousSlide = self.slides[currentIndex - 1]
                previousSlide.imageView.transform =  CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
            if currentIndex + 1 < self.images.count {
                let nextSlide = self.slides[currentIndex + 1]
                nextSlide.imageView.transform =  CGAffineTransform(scaleX: 0.8, y: 0.8)
            }
        }
    }
    @IBAction func skipIntro(_ sender:Any){
        let offsetWidth = scrollView.frame.size.width * CGFloat(slides.count - 1)
        scrollView.setContentOffset(CGPoint(x: offsetWidth, y: 0), animated: false)
    }
}
extension IntroStartTourViewController : UIViewStyling {
    func setupViews() {
        scrollView.delegate = self
        slides = createSlides()
        setupSlideScrollView(slides: slides)
        pageControl.numberOfPages = slides.count
        pageControl.currentPage   = 0
        view.bringSubview(toFront: pageControl)
    }
    private func createSlides() -> [Slide] {
            for i in 0..<images.count{
                let slide = Bundle.main.loadNibNamed("Slide", owner: self, options: nil)?.first as! Slide
                slide.imageView.image = images[i]
                slide.title.text      = titles[i]
                slide.subtitle.text   = subtitles[i]
                slide.index           = i
                slides.insert(slide, at: i)
            }
            return slides
        }
    private func setupSlideScrollView(slides : [Slide]) {
            let optimalHeight = self.view.frame.height - scrollView.frame.origin.y - 64
            scrollView.frame  = CGRect(x: 0, y: 0, width: view.frame.width, height: optimalHeight)
            scrollView.contentSize = CGSize(width: view.frame.width * CGFloat(slides.count), height: scrollView.frame.height)
            scrollView.isPagingEnabled = true
            
            for i in 0 ..< slides.count {
                slides[i].frame = CGRect(x: view.frame.width * CGFloat(i), y: 0, width: view.frame.width, height: scrollView.frame.height)
                
                if i == slides.count - 1 {
                    slides[i].buttonStart.isHidden = false
                }
                scrollView.addSubview(slides[i])
            }
        }
}
