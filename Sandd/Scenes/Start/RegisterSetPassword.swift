//
//  RegisterSetPasswordViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol RegisterSetPasswordView : BaseView {
    var onPressBack : (()->Void)? {get set}
    var onPressSetPersonData : (()->Void)? {get set}
}
class RegisterSetPasswordViewController: BaseViewController , RegisterSetPasswordView{
    var onPressSetPersonData: (() -> Void)?
    var onPressBack: (() -> Void)?

    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var textFieldRepeatPassword: UITextField!
    @IBOutlet weak var textFiledPassword: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func didPressBack(_ sender: Any){
        onPressBack?()
    }
    @IBAction func didPressNexSetPersonData(_ sender: Any){
        let checkEmail = EmailAndPasswordCheck().checkPasswordRepeatPassword(password: textFiledPassword.text, repeatedPassword: textFieldRepeatPassword.text)
        if checkEmail == (true,nil) {
            updateUserCredentials()
            onPressSetPersonData?()
        }else {
            self.showAllert(title: "Mislukt", message: checkEmail.1!)
        }
    }
    private func updateUserCredentials(){
        if var user = UserCredentials().getUserCredentials(){
            user.password = textFiledPassword.text
            user.saveUserCredentials()
        }
    }
}
extension RegisterSetPasswordViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        setupKeyboard()
    }
    
    func setupKeyboard(){
        customizeKeyboard(button: buttonContinue, textFields: [textFiledPassword,textFieldRepeatPassword])
        continueButton.addTarget(self, action: #selector(didPressNexSetPersonData(_:)), for: .touchUpInside)
    }

}
