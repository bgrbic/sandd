//
//  MoreViewModel.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol MoreViewModelProtocol {
    func getElements( result : (_ elements : [ModelMore])->Void)
}
struct MoreViewModel  : MoreViewModelProtocol{
    
    func getElements(result: ([ModelMore]) -> Void) {
        let elements = [ModelMore(title: "Bestellingen", image:"imageBestellingen"),
                        ModelMore(title: "Videos", image:"imageVideos"),
                        ModelMore(title: "Instellingen", image:"imageInstellingen"),
                        ModelMore(title: "Over deze app", image:"imageOverDezeApp"),
                        ModelMore(title: "Feedback", image:"imageFeedback"),
                        ModelMore(title: "Uitloggen", image:"imageUitloggen")]
        result(elements)
    }
}
