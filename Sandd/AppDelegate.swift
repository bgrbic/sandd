//
//  AppDelegate.swift
//  Sandd
//
//  Created by Branko Grbic on 8/10/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit
import IQKeyboardManager

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    var networkReachable : Reachability!
    var locationManager  = CoreLocationManager()
    
    lazy var applicationCoordinator: Coordinator  = {
        return self.makeCoordinator()
    }()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        IQKeyboardManager.shared().isEnabled = true
        setupReachability()
        setupLocationTracking()


        //not selected
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor.red, NSAttributedStringKey.font : UIFont(name:"ProximaNovaSoft-Medium", size: 12) as Any], for: .normal)
        
        //selected
        UITabBarItem.appearance().setTitleTextAttributes([NSAttributedStringKey.foregroundColor: UIColor(r: 230, g: 0, b: 110, alpha: 1),NSAttributedStringKey.font : UIFont(name:"ProximaNovaSoft-Medium", size: 12) as Any], for: .selected)
        
        
        let notification = launchOptions?[.remoteNotification] as? [String: AnyObject]
        let deepLink = LauncerManagerOption.build(with: notification)
        applicationCoordinator.start(with: deepLink)
        return true
    }
    func applicationWillEnterForeground(_ application: UIApplication) {
        locationManager.checkStatus()
    }
}
extension AppDelegate {
    private var rootController: UINavigationController {
        return self.window!.rootViewController as! UINavigationController
    }
    private func makeCoordinator() -> Coordinator {
        return AppCoordinator(
            router: RouterImp(rootController: self.rootController),
            coordinatorFactory: CoordinatorFactoryImp()
        )
    }
}
extension AppDelegate : RemoteNotificationsCycle{
    func application(_ application: UIApplication,
                     didReceiveRemoteNotification userInfo: [AnyHashable : Any]) {
        let dict = userInfo as? [String: AnyObject]
        let deepLink = LauncerManagerOption.build(with: dict)
        applicationCoordinator.start(with: deepLink)
    }
    func application(_ application: UIApplication, continue userActivity: NSUserActivity,
                     restorationHandler: @escaping ([Any]?) -> Void) -> Bool {
        let deepLink = LauncerManagerOption.build(with: userActivity)
        applicationCoordinator.start(with: deepLink)
        return true
    }
    func application(_ application: UIApplication, didRegisterForRemoteNotificationsWithDeviceToken deviceToken: Data) {
        //TODO: send device token to server
    }
}
