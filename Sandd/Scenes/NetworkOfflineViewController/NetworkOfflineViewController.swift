//
//  NetworkOfflineViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/21/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class NetworkOfflineViewController: UIViewController {
    @IBOutlet weak var imageLoading: UIImageView!
    @IBOutlet weak var labelOfflineTitle: UILabel!
    @IBOutlet weak var labelOfflineSubtitle: UILabel!
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        imageLoading.makeRound()
    }
    override func viewWillAppear(_ animated: Bool) {
        self.imageLoading.rotate()
    }
    @IBAction func openSettings(_ sender : Any) {
        if let url = NSURL(string: UIApplicationOpenSettingsURLString) as URL? {
            UIApplication.shared.open(url,completionHandler: nil)
        }
    }
}
extension UIView{
    func rotate() {
        let rotation : CABasicAnimation = CABasicAnimation(keyPath: "transform.rotation.z")
        rotation.toValue = NSNumber(value: Double.pi * 2)
        rotation.duration = 1
        rotation.isCumulative = true
        rotation.repeatCount = Float.greatestFiniteMagnitude
        self.layer.add(rotation, forKey: "rotationAnimation")
    }
}
