//
//  StartCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation
enum StartOutputScreen : Int{
   case showSplash = 0,showIntroVideo,showSignUp,showLogin,showForgotPassword,sendResetPasswordEmail,showNewPassword,showRegister,showSetPassword,
       showSetPersonalData , showStartTour ,showIntro ,showVacancies
}
protocol StartCoordinatorOutput : class {
    var finishFlow: (() -> Void)? { get set }
}
class StartCoordinator : BaseCoordinator , StartCoordinatorOutput {
    var finishFlow: (() -> Void)?
    
    private var factory : StartModuleFactory
    private var router  : Router
    private var startWithScreen : StartOutputScreen!
    
    init(router: Router, factory: StartModuleFactory,startWithScreen: StartOutputScreen){
        self.factory    = factory
        self.router     = router
        self.startWithScreen = startWithScreen
    }
    override func start() {
        if let startWith = StartOutputScreen(rawValue: startWithScreen!.rawValue) {
            switch startWith {
            case .showSplash:
                showSplash()
            case .showSignUp:
                showSignUp(currentVideoTime: 0.0)
            default:
                print("other jump here")
            }
        }
    }
    private func showSplash(){
        let splashOutput = factory.makeSplashOutput()
        splashOutput.onFailedAuthorize = { [weak self] in
            self?.showIntroVideo()
        }
        splashOutput.onSuccessAuthorize = { [weak self] in
            self?.finishFlow?()
        }
        router.setRootModule(splashOutput, hideBar: true)
    }
    private func showIntroVideo(){
        let videoIntroOutput =  factory.makeIntroVideoOutput()
        videoIntroOutput.onSwipe = { [weak self] (currentVideoTime) in
            self?.showSignUp(currentVideoTime: currentVideoTime)
        }
        router.push(videoIntroOutput, animated: false)
    }
    private func showSignUp(currentVideoTime : Float64?){
        let signupOutput = factory.makeSignUpOutput(currentVideoTime: currentVideoTime)
        signupOutput.onLoginClick = { [weak self] in
             self?.showLogin()
        }
        signupOutput.onRegisterClick = { [weak self] in
             self?.showRegister()
        }
        signupOutput.onFindeVacancies = { [weak self] in
             self?.showVacancies()
        }
        router.setRootModule(signupOutput, hideBar: true, animated: false)
    }
    private func showLogin(){
        let loginOutput = factory.makeLoginOutput()
        loginOutput.didPressForgotPassword = { [weak self] in
            self?.showForgotPassword()
        }
        loginOutput.didPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        loginOutput.didSucessLogin = { [weak self] in
            self?.finishFlow?()
        }
        router.push(loginOutput, animated: true)
    }
    private func showForgotPassword(){
        let forgotPassOutput = factory.makeForgotPasswordOutput()
        forgotPassOutput.onPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        forgotPassOutput.onPressSendResetEmail = { [weak self] in
            self?.sendResetPasswordEmail()
        }
        router.push(forgotPassOutput, animated: true)
    }
    private func sendResetPasswordEmail(){
        //TODO: here call manager to send email for credentials or before in VC
        showNewPassword()
    }
    private func showNewPassword(){
        let newPasswordOutput = factory.makeNewPasswordOutput()
        newPasswordOutput.onPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        newPasswordOutput.onSuccessLogin = { [weak self] in
            self?.finishFlow?()
        }
        router.push(newPasswordOutput, animated: true)
    }
    private func showRegister(){
        let registerOutput = factory.makeRegisterOutput()
        registerOutput.onPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        registerOutput.onPressSetPassword = {[weak self] in
            self?.showSetPassword()
        }
        router.push(registerOutput, animated: true)
    }
    private func showSetPassword(){
        let setPasswordOutput = factory.makeRegisterSetPasswordOutput()
        setPasswordOutput.onPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        setPasswordOutput.onPressSetPersonData = { [weak self] in
            self?.showSetPersonalData()
        }
        router.push(setPasswordOutput, animated: true)
    }
    private func showSetPersonalData(){
        let personalDataOutput = factory.makeRegisterPersonalDataOutput()
        personalDataOutput.onPressBack = { [weak self] in
            self?.router.popModule(animated: true)
        }
        personalDataOutput.onPressStartTour = {[weak self] in
            self?.showStartTour()
        }
        router.push(personalDataOutput, animated: true)
    }
    private func showStartTour(){
        let showTourOutput = factory.makeRegisterStartTourOutput()
        showTourOutput.onPressShowIntro = { [weak self] in
            self?.showIntro()
        }
        router.setRootModule(showTourOutput, hideBar: true)
    }
    private func showIntro(){
        let showIntroOutput = factory.makeIntroStartTourOutput()
        showIntroOutput.showHome = { [weak self] in
            self?.finishFlow?()
        }
        router.push(showIntroOutput, animated: true)
    }
    private func showVacancies(){
        let vacanciesOutput = factory.makeFindeVacanciesOuput()
        vacanciesOutput.onDismissVacancies = { [weak self] in
            self?.router.dismissModule(animated: true, completion: nil)
        }
        router.present(vacanciesOutput, animated: true)
    }
}
