//
//  RegisterStepTwo.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class RegisterSetPassword: UIViewController,UITextFieldDelegate {
    lazy var viewModel : ViewModelRegisterSetPassword = {
        let viewModel = ViewModelRegisterSetPassword(superview:self.view)
        return viewModel
    }()
    
    var activeTextField: UITextField?
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
    }
    
    
    @IBAction func actionBack(_ sender: UIBarButtonItem) {
        activeTextField?.resignFirstResponder()
        self.navigationController?.popViewController(animated: true)
    }
}

