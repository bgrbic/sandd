//
//  LoginViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/17/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit
import IQKeyboardManager
protocol LoginView : BaseView {
    var didPressForgotPassword : (()->Void)? {get set}
    var didPressBack   : (()->Void)? {get set}
    var didSucessLogin : (()->Void)? {get set}
}
class LoginViewController: BaseViewController , LoginView{
    @IBOutlet weak var textFieldEmail       : UITextField!
    @IBOutlet weak var textFieldPassword    : UITextField!
    @IBOutlet weak var buttonLogin: UIButton!
    @IBOutlet weak var viewNavigationFooter: UIView!
    @IBOutlet weak var labelInfo: UILabel!
    
    var didSucessLogin: (() -> Void)?
    var didPressBack:   (() -> Void)?
    var didPressForgotPassword: (() -> Void)?
    
    private var modelView : LoginViewModelProtocol?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupKeyboard()
        modelView = LoginViewModel()
        modelView?.getUserCredentials(user: { (user) in
              textFieldEmail.text    = user?.email
              textFieldPassword.text = user?.password
        })
        
        //MARK: TODO: FIX BUG WITH XCODE NOT READING ALL CODES IN
        for family: String in UIFont.familyNames
        {
            print("\(family)")
            for names: String in UIFont.fontNames(forFamilyName: family)
            {
                print("== \(names)")
            }
        }
    }
    func setupKeyboard(){
        customizeKeyboard(button: buttonLogin, textFields: [textFieldEmail,textFieldPassword])
        continueButton.addTarget(self, action: #selector(didPressLogin(_:)), for: .touchUpInside)
        
        let attributedString = NSMutableAttributedString(string: "Door in te loggen ga je akkoord met onze \nPrivacybeleid en Algemene voorwaarden", attributes: [
            .font: UIFont(name: "OpenSans", size: 14.0)!,
            .foregroundColor: UIColor(r: 128, g: 128, b: 128, alpha: 1),
            .kern: 0.25
            ])
        attributedString.addAttribute(.font, value: UIFont(name: "OpenSans-Semibold", size: 14.0)!, range: NSRange(location: 42, length: 13))
        attributedString.addAttribute(.font, value: UIFont(name: "OpenSans-Semibold", size: 14.0)!, range: NSRange(location: 59, length: 20))
        labelInfo.attributedText = attributedString
    }
    @IBAction func didPressForgotPassword(sender : Any){
        didPressForgotPassword?()
    }
    @IBAction func didPressBackButton(_ sender:Any){
        self.navigationController?.isNavigationBarHidden = true 
        didPressBack?()
    }
    @IBAction func didPressLogin(_ sender: Any){
        let validEmailAndPassword = EmailAndPasswordCheck().passwordAndEmailCheck(email: textFieldEmail.text, password: textFieldPassword.text)
        if validEmailAndPassword {
            didSucessLogin?()
        }else {
            self.showAllert(title: "Aanmelden mislukt", message: "Controleer uw wachtwoord en e-mailadres")
        }
    }
}
extension LoginViewController {
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.navigationController?.isNavigationBarHidden = false
    }

}
