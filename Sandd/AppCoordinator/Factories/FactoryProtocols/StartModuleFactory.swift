//
//  StartModuleFactory.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol StartModuleFactory {
    
    func makeSplashOutput()->SplashView
    func makeIntroVideoOutput()->IntroVideoView
    func makeSignUpOutput(currentVideoTime : Float64?)->SignUpView
    
    func makeLoginOutput()->LoginView
    func makeNewPasswordOutput()->NewPasswordView
    func makeForgotPasswordOutput()->ForgotPasswordView
    
    func makeRegisterOutput()->RegisterView
    func makeRegisterSetPasswordOutput()->RegisterSetPasswordView
    func makeRegisterPersonalDataOutput()->RegisterPersonalDataView
    func makeRegisterStartTourOutput()->RegisterStartTourView
    
    func makeFindeVacanciesOuput()->FindeVacanciesView
    func makeIntroStartTourOutput()->IntroStartTourView
}
