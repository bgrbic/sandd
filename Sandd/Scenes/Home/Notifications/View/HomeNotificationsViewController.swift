//
//  HomeNotificationsViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol HomeNotificationsView : BaseView{
    
}
class HomeNotificationsViewController: UIViewController , HomeNotificationsView {
    override func viewDidLoad() {
        super.viewDidLoad()
        print("HomeNotificationsViewController")
    }
}
