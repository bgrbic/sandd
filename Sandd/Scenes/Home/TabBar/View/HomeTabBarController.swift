//
//  HomeTabBarController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

fileprivate enum TabBarControllersIndex : Int {
    case planing = 0, performance, chat, notifications, more
}
protocol HomeTabBarCoordinatorOutput : class {
    var logoutUser: (() -> Void)? { get set }
}
protocol HomeTabBarView : BaseView , HomeTabBarCoordinatorOutput{
    var onPlanningSelectItem      : ((UINavigationController)->Void)? {get set}
    var onPerformanceSelectItem   : ((UINavigationController)->Void)? {get set}
    var onChatSelectItem          : ((UINavigationController)->Void)? {get set}
    var onNotificationsSelectItem : ((UINavigationController)->Void)? {get set}
    var onMoreSelectItem          : ((UINavigationController)->Void)? {get set}
    var onViewDidLoad             : ((UINavigationController)->Void)? {get set}
    var logoutUser                : (() -> Void)? {get set}
}
class HomeTabBarController: UITabBarController , HomeTabBarView {
   
    var logoutUser: (() -> Void)?
    
    var onPlanningSelectItem      : ((UINavigationController) -> Void)?
    var onPerformanceSelectItem   : ((UINavigationController) -> Void)?
    var onChatSelectItem          : ((UINavigationController) -> Void)?
    var onNotificationsSelectItem : ((UINavigationController) -> Void)?
    var onMoreSelectItem          : ((UINavigationController) -> Void)?
    var onViewDidLoad             : ((UINavigationController) -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        delegate = self
        if let controller = customizableViewControllers?.first as? UINavigationController {
            onViewDidLoad?(controller)
        }
        setupCoordinatorHandlers()
    }
    private func setupCoordinatorHandlers(){
        if let nav = viewControllers?.last as? UINavigationController , let moreView = nav.viewControllers.last as? HomeMoreView {
            moreView.didSelectLogout = { [weak self] in
                self?.logoutUser?()
            }
        }
    }
}
extension HomeTabBarController : UITabBarControllerDelegate {
    func tabBarController(_ tabBarController: UITabBarController, didSelect viewController: UIViewController) {
        guard let controller = viewControllers?[selectedIndex] as? UINavigationController else { return }
        
        switch selectedIndex {
        case TabBarControllersIndex.planing.rawValue:
            onPlanningSelectItem?(controller)
        case TabBarControllersIndex.performance.rawValue:
            onPerformanceSelectItem?(controller)
        case TabBarControllersIndex.chat.rawValue:
            onChatSelectItem?(controller)
        case TabBarControllersIndex.notifications.rawValue:
            onNotificationsSelectItem?(controller)
        case TabBarControllersIndex.more.rawValue:
            onMoreSelectItem?(controller)
        default:
            print("other controller selected on HomeTabBarController")
        }
    }
}
