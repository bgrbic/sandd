//
//  ForgotPasswordViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol ForgotPasswordView : BaseView {
    var onPressBack         : (()->Void)? {get set}
    var onPressSendResetEmail    : (()->Void)? {get set}
}
class ForgotPasswordViewController: BaseViewController , ForgotPasswordView {
    
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var textFieldEmail: UITextField!
    
    var onPressSendResetEmail: (() -> Void)?
    var onPressBack: (() -> Void)?

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func didPressBackButton(_ sender: Any){
        onPressBack?()
    }
    private func saveUserCredentials(){
        let user = UserCredentials(email: textFieldEmail.text, password: nil, phone: nil, adress: nil, code: nil)
        user.saveUserCredentials()
    }
    @IBAction func didPressSendEmail(_ sender: Any){
        if EmailAndPasswordCheck().validateEmail(email: textFieldEmail.text!) {
            saveUserCredentials()
            onPressSendResetEmail?()
            return
        }
        self.showAllert(title: "Mislukt", message: "Ongeldige e-mail")
    }
}
extension ForgotPasswordViewController {
    override func viewWillAppear(_ animated: Bool) {
        setupKeyboard()
    }
    private func setupKeyboard(){
        customizeKeyboard(button: buttonContinue, textFields: [textFieldEmail])
        continueButton.addTarget(self, action: #selector(didPressSendEmail(_:)), for: .touchUpInside)
    }

}
