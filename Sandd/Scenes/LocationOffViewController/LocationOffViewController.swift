//
//  LocationOffViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/21/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

class LocationOffViewController: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func openLocation(_ sender : Any) {
        //MARK - Can't use scheme url = "App-prefs:root=LOCATION_SERVICES" !!!
        //Apple will reject if we use directly private entity ! General is all we got
        if let url = NSURL(string: UIApplicationOpenSettingsURLString) as URL? {
            UIApplication.shared.open(url,completionHandler: nil)
        }
    }
}
