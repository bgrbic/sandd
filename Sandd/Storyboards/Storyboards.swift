//
//  StoryboardsEnum.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

enum Storyboards: String {
    case start       = "Start"
    case home        = "Home"
}
