//
//  FindeVacanciesViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol FindeVacanciesView : BaseView {
    var onDismissVacancies : (()->Void)? {get set}
}
class FindeVacanciesViewController: BaseViewController ,FindeVacanciesView {
    
    var onDismissVacancies: (() -> Void)?
    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
    @IBAction func didPressBack(_ sender: Any) {
        onDismissVacancies?()
    }
}
extension FindeVacanciesViewController : UIViewStyling {
    func setupViews() {
        webView.delegate = self
        guard let url = URL(string: ServerAPI.shared.findeVacancies)else { return }
        let urlRequest = URLRequest(url: url)
        webView.loadRequest(urlRequest)
    }
}
extension FindeVacanciesViewController : UIWebViewDelegate {
    func webView(_ webView: UIWebView, didFailLoadWithError error: Error) {
        webView.stopLoading()
        print("webView didFailLoadWithError : \(error.localizedDescription)")
    }
}
