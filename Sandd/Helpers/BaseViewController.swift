//
//  BaseViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit
import IQKeyboardManager

class BaseViewController: UIViewController {
    var continueButton  = UIButton()
    var activeTextField = UITextField()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: UIBarMetrics.default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
    }
    override var description: String {
        return "\(type(of: self))"
    }
}
//MARK:- Alert
extension BaseViewController {
    func showAllert(title: String, message: String) {
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
        self.present(alert, animated: true)
    }
}
//MARK:- TextField Delegate
extension BaseViewController: UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        activeTextField = textField
        activeTextField.textContentType = UITextContentType("")
    }
    func textFieldShouldBeginEditing(_ textField: UITextField) -> Bool {
        activeTextField = textField
        return true
    }
    func textFieldDidEndEditing(_ textField: UITextField) {
        activeTextField.resignFirstResponder()
    }
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        self.view.endEditing(true)
        return false
    }
}
extension BaseViewController {
    func customizeKeyboard(button : UIButton,textFields:[UITextField]){
        IQKeyboardManager.shared().shouldResignOnTouchOutside = true
        IQKeyboardManager.shared().shouldShowToolbarPlaceholder = false
        IQKeyboardManager.shared().toolbarDoneBarButtonItemText = ""
        IQKeyboardManager.shared().keyboardDistanceFromTextField = 20
        
        // to remove the IQManager view on top of Keyboard
        let emptyUIView = UIButton()
        emptyUIView.setBackgroundImage(button.currentBackgroundImage, for: .normal)
        emptyUIView.titleLabel?.textColor = .white
        emptyUIView.setTitle(button.titleLabel?.text, for: .normal)
        emptyUIView.frame = button.frame
        for textField in textFields {
            textField.delegate = self
            textField.inputAccessoryView = emptyUIView
        }
        continueButton = emptyUIView
    }
}

//MARK:- Handling Keyboard subview button
extension BaseViewController {
    override func viewWillAppear(_ animated: Bool) {
        NotificationCenter.default.addObserver(self, selector: #selector(self.keyboardDidHide), name: Notification.Name.UIKeyboardDidHide, object: nil)
    }
    @objc func keyboardDidHide(notification: Notification) {
        self.continueButton.removeFromSuperview()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        NotificationCenter.default.removeObserver(self)
    }
}
