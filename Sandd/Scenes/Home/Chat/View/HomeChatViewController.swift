//
//  HomeChatViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/19/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol HomeChatView : BaseView{
    
}
class HomeChatViewController: UIViewController , HomeChatView{

    override func viewDidLoad() {
        super.viewDidLoad()
        print("HomeChatViewController")
    }
}
