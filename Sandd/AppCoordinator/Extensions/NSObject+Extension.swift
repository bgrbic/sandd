//
//  NSObject+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

extension NSObject {
    
    class var nameOfClass: String {
        return NSStringFromClass(self).components(separatedBy: ".").last!
    }
}
