//
//  HomePerformanceCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol HomePerformanceModulFactory {
    func makeHomePerformanceOutput()->HomePerformanceView
}
class HomePerformanceCoordinator : BaseCoordinator{
    
    private var router : Router
    private var coordinatorFactory : CoordinatorFactory
    private var factory : HomeModuleFactory
    
    init(router : Router,factory: HomeModuleFactory,coordinatorFactory : CoordinatorFactory){
        self.factory = factory
        self.router  = router
        self.coordinatorFactory = coordinatorFactory
    }
    override func start() {
        print("HomePerformanceCoordinator")
    }
}
extension HomePerformanceCoordinator {
    
}
