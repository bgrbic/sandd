//
//  UserCredentials.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

//MARK - model for save/get user credentials during test workflow
struct UserCredentials : Codable {
    private static  let UserCredentialsKey = "UserCredentials"
    
    var name     : String?
    var lastName : String?
    var email    : String?
    var password : String?
    var phone    : String?
    var adress   : String?
    var code     : String?
    
    init(email : String? = "",password:String? = "",phone : String? = "",adress: String? = "",code : String? = "",name : String? = "",lastName : String? = "")
    {
        self.email      = email
        self.password   = password
        self.phone      = phone
        self.adress     = adress
        self.code       = code
        self.name       = name
        self.lastName   = lastName
    }
    func saveUserCredentials(){
        let encoder = JSONEncoder()
        if let encoded = try? encoder.encode(self) {
            let ud = UserDefaults.standard
            ud.set(encoded, forKey: UserCredentials.UserCredentialsKey)
        }
    }
    func getUserCredentials()->UserCredentials? {
        let ud = UserDefaults.standard
        if let saveUserData = ud.object(forKey: UserCredentials.UserCredentialsKey) as? Data {
            let decoder = JSONDecoder()
            if let loadedUser = try? decoder.decode(UserCredentials.self, from: saveUserData) {
                return loadedUser
            }
        }
        return nil
    }
    static func clearUser(){
        let ud = UserDefaults.standard
        ud.setValue(nil, forKey: UserCredentialsKey)
    }
}
