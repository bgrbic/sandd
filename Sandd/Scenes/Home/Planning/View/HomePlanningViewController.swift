//
//  HomePlanningViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol HomePlanningView : BaseView {}

class  HomePlanningViewController : BaseViewController , HomePlanningView{
    
    @IBOutlet weak var lblWeather: UILabel!
    @IBOutlet weak var viewNavfooter: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        setupViews()
    }
}
extension HomePlanningViewController : UIViewStyling {
    func setupViews() {
        lblWeather.backgroundColor    = UIColor(r: 152, g: 161, b: 178, alpha: 1)
        viewNavfooter.backgroundColor = UIColor(r: 0, g: 60, b: 152, alpha: 1)
        lblWeather.makeRound()
    }
}
