//
//  NewPasswordViewController.swift
//  Sandd
//
//  Created by Branko Grbic on 8/20/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

protocol NewPasswordView : BaseView {
    var onPressBack : (()->Void)?    {get set}
    var onSuccessLogin : (()->Void)? {get set}
}
class NewPasswordViewController: BaseViewController , NewPasswordView{
   
    @IBOutlet weak var buttonContinue: UIButton!
    @IBOutlet weak var textFieldRepeatNewPassword: UITextField!
    @IBOutlet weak var textFieldCode        : UITextField!
    @IBOutlet weak var textFieldNewPassword : UITextField!
    
    var onSuccessLogin: (() -> Void)?
    var onPressBack: (() -> Void)?
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    @IBAction func didPressBackButton(_ sender : Any){
        onPressBack?()
    }
    @IBAction func didPressLogin(_ sender: Any){
        let checkPasswords = EmailAndPasswordCheck().checkPasswordRepeatPassword(password: textFieldNewPassword.text, repeatedPassword: textFieldRepeatNewPassword.text)
        if  checkPasswords == (true,nil){
            updateUserCredentials()
            onSuccessLogin?()
            return
        }
        self.showAllert(title: "Fout", message: checkPasswords.1!)
    }
    private func updateUserCredentials(){
        if var user = UserCredentials().getUserCredentials(){
               user.code     = textFieldCode.text
               user.password = textFieldNewPassword.text
               user.saveUserCredentials()
        }
    }
}
extension NewPasswordViewController {
    func setupKeyboard(){
        customizeKeyboard(button: buttonContinue, textFields: [textFieldCode,textFieldNewPassword,textFieldRepeatNewPassword])
        continueButton.addTarget(self, action: #selector(didPressLogin(_:)), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        setupKeyboard()
    }
}
