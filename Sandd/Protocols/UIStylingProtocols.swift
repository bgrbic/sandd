//
//  UIStylingProtocols.swift
//  Sandd
//
//  Created by Branko Grbic on 8/2/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol UIViewStyling {
    func setupViews()
}
protocol UIConstraintStyling{
    func setupConstraints()
}
protocol UIStyleCollectionView {
    func configure()
}
protocol UIStyling : UIViewStyling, UIConstraintStyling{ }
