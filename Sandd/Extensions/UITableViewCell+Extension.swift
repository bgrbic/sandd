//
//  UITableViewCell+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

extension UITableViewCell {
    static var reuseIdentifier: String {
        return String(describing: self)
    }
    func hideSeparator() {
        self.separatorInset = UIEdgeInsets(top: 0, left: self.bounds.size.width, bottom: 0, right: 0)
    }
    
    func showSeparator() {
        self.separatorInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    
}
protocol TableViewCellIdentificator {
    static var cellIdentifier: String { get }
}
extension TableViewCellIdentificator where Self: UITableViewCell {
    static var cellIdentifier: String {
        return String(describing: self)
    }
}
