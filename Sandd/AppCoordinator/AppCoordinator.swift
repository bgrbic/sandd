//
//  AppCoordinator.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

private var isAutorized     = false
private var isLogoutPressed = false

private enum LaunchManager {
    case start,home
    
    static func configure(
        isAutorized: Bool = isAutorized) -> LaunchManager {
        
        switch (isAutorized) {
            case (false): return .start
            case (true) : return .home
        }
    }
}
class AppCoordinator: BaseCoordinator {
    
    private let coordinatorFactory: CoordinatorFactory
    private let router: Router
    
    private var launcher : LaunchManager {
        return LaunchManager.configure()
    }
    init(router: Router, coordinatorFactory: CoordinatorFactory) {
        self.router = router
        self.coordinatorFactory = coordinatorFactory
    }
    override func start(with option: LauncerManagerOption?) {
        if let option = option {
            switch option {
            case .start: runStartFlow()
            case .home:  runHomeFlow()
            default: childCoordinators.forEach { coordinator in
                coordinator.start(with: option)
                }
            }
        } else {
            switch launcher {
                case .start: runStartFlow()
                case .home:  runHomeFlow()
            }
        }
    }
    private func runStartFlow(){
        let startWithScreen = isLogoutPressed ? StartOutputScreen.showSignUp : StartOutputScreen.showSplash
        
        let coordinator = coordinatorFactory.makeStartCoordinator(router: router, startWithScrenn: startWithScreen)
        coordinator.finishFlow = { [weak self, weak coordinator] in
            isAutorized     = true
            isLogoutPressed = false
            self?.start()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        coordinator.start()
    }
    private func runHomeFlow(){
        let (coordinator,module) = coordinatorFactory.makeHomeTabBarCoordinator()
        coordinator.logoutUser = { [weak self , weak coordinator] in
            UserCredentials.clearUser()
            isAutorized     = false
            isLogoutPressed = true
            self?.start()
            self?.removeDependency(coordinator)
        }
        addDependency(coordinator)
        router.setRootModule(module, hideBar: true)
        coordinator.start()
    }
}
