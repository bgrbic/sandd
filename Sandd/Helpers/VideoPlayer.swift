//
//  VideoPlayer.swift
//  Sandd
//
//  Created by Branko Grbic on 8/1/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import AVFoundation
import UIKit

private enum VideoErrors : String, Error{
    case VideoDoesntExist = "Video doesn't exist . Check path to video"
}
protocol VideoPlayerProtocol {
    var videoLayer  : AVPlayerLayer? {get set}
    var opacity     : Float          {get set}
    init?(superView : UIView,videoURL : URL?)
    func stopPlaying()
    func startPlaying()
    func removePlayer()
}
class VideoPlayer {
    private var _opacity              : Float = 0.8
    private var player                : AVQueuePlayer!
    private var playerLoop            : AVPlayerLooper!
    var videoLayer                    : AVPlayerLayer?
    
    var opacity : Float {
        get {
            return _opacity
        }set{
            _opacity = newValue >= 0 && newValue < 1.0 ? newValue : 1.0
            videoLayer?.opacity = _opacity
        }
    }
    var currentMediaTimePlayback : Float64? {
        get {
            if let duration = player.currentItem?.currentTime().seconds {
                let seconds = Float64(duration)
                return seconds
            }
            return nil
        }set {
            if let _mediaTime = newValue {
                delay(seconds: 0.1){ [weak self] in
                    guard let _player = self?.player else { return }
                    let seekToTime = CMTime(value: Int64(_mediaTime), timescale: _player.currentTime().timescale)
                        _player.currentItem?.seek(to: seekToTime, toleranceBefore: kCMTimeZero, toleranceAfter: kCMTimeZero, completionHandler: nil)
                }
            }
        }
    }
    required init?(superView : UIView,videoURL : URL?) {
        guard let _videoPath = videoURL else  {
            print(VideoErrors.VideoDoesntExist.localizedDescription)
            return nil
        }
        loadVideo(superView: superView, videoURL: _videoPath)
    }
    required init?(coder aDecoder: NSCoder) {
        debugPrint("init(coder:) has not been implemented")
    }
}
extension VideoPlayer : VideoPlayerProtocol {

    private func loadVideo(superView: UIView,videoURL : URL!){
        player = AVQueuePlayer()
        player.externalPlaybackVideoGravity = AVLayerVideoGravity.resizeAspectFill
        
        let playerItem = AVPlayerItem(url: videoURL)
        playerLoop     = AVPlayerLooper(player: player, templateItem: playerItem)
        player.insert(playerItem, after: nil)
        
        videoLayer               = AVPlayerLayer(player: player)
        videoLayer?.frame        = superView.bounds
        videoLayer?.opacity      = opacity
        videoLayer?.videoGravity = .resizeAspectFill
    }
    func stopPlaying(){
        guard let _player = player else {return }
        _player.pause()
    }
    func startPlaying(){
        guard let _player = player else { return }
        _player.volume = 0.1
        _player.play()
    }
    func removePlayer(){
        guard let _player = player else {return }
        _player.replaceCurrentItem(with: nil)
        playerLoop.disableLooping()
    }
}
