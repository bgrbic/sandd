//
//  ModelMoreElements.swift
//  Sandd
//
//  Created by Branko Grbic on 8/24/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol ModelMoreProtocol {
    func checkIfTitleEmpty()->Bool
    func checkIfImageEmpty()-> Bool
}
struct ModelMore {
    let title : String!
    let image : String!
    init(title:String,image:String) {
        self.title = title
        self.image = image
    }
    func checkIfTitleEmpty()->Bool {
        guard let _  = title else { return false }
        return true
    }
    func checkIfImageEmpty()-> Bool{
        guard let _ = image else  { return false }
        return true
    }
}
