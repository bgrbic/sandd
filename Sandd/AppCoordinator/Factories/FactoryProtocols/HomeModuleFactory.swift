//
//  HomeModuleFactory.swift
//  Sandd
//
//  Created by Branko Grbic on 8/18/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

protocol HomeModuleFactory {
    func makeHomeTabBarOutput()->HomeTabBarView
    func makeHomePerformanceOutput()->HomePerformanceView
    func makeHomeChatOutput()->HomeChatView
    func makeHomePlanningOutput()->HomePlanningView
    func makeHomeNotificationOutput()->HomeNotificationsView
    func makeHomeMoreOutput()->HomeMoreView
}
