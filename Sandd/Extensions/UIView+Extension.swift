//
//  UIView+Extension.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

extension UIView {
    func makeRound() {
        self.layer.cornerRadius  = self.frame.size.height/2
        self.layer.masksToBounds = true
    }
}
