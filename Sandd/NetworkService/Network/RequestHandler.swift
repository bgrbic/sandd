//
//  RequestHandler.swift
//  Sandd
//
//  Created by Branko Grbic on 8/15/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import Foundation

class RequestHandler {
    
    let reachability = Reachability()!
    let networkQueue = DispatchQueue(label: "RequestHandler.networkQueue", qos: .utility, attributes: .concurrent, autoreleaseFrequency: .inherit, target: nil)
    
    func networkResult<T : Decodable>(completion: @escaping ((Result<[T], ErrorResult>) -> Void)) ->
        ((Result<Data, ErrorResult>) -> Void) {
            
            return {  dataResult in
                self.networkQueue.async(execute: {
                    switch dataResult {
                    case .success(let data) :
                        ParserHelper.parse(data: data, completion: completion)
                        break
                    case .failure(let error) :
                        mainThread {
                              completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                        }
                        break
                    }
                })
            }
    }
    func networkResult<T: Decodable>(completion: @escaping ((Result<T, ErrorResult>) -> Void)) ->
        ((Result<Data, ErrorResult>) -> Void) {
            
            return { dataResult in
                self.networkQueue.async(execute: {
                    switch dataResult {
                    case .success(let data) :
                        ParserHelper.parse(data: data, completion: completion)
                        break
                    case .failure(let error) :
                        mainThread {
                            completion(.failure(.network(string: "Network error " + error.localizedDescription)))
                        }
                        break
                    }
                })
            }
    }
}

