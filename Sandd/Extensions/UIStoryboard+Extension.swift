//
//  UIStoryboard+Extension.swift
//  Sandd
//
//  Created by Branko Grbic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit

enum Storyboard : String {
    case start     = "Start"
    case home      = "Home"
}
extension UIStoryboard {
    static func loadFromStart(_ identifier: String) -> UIViewController? {
        return load(from: .start, identifier: identifier)
    }
    static func load(from storyboard: Storyboard, identifier: String) -> UIViewController? {
        let storyboard = UIStoryboard(name: storyboard.rawValue, bundle: nil)
        return storyboard.instantiateViewController(withIdentifier: identifier)
    }
}
