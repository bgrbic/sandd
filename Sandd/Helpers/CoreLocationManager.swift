//
//  CoreLocationManager.swift
//  Sandd
//
//  Created by Branko Grbic on 8/21/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import CoreLocation

protocol CoreLocationManagerProtocol {
    var whenLocationEnable   : (()->Void)? {get set}
    var whenLocationDisable  : (()->Void)? {get set}
    var locationUpdateResult : ((_ locations: [CLLocation]? , _ error : Error? )->Void)? {get set}
}
class CoreLocationManager : NSObject , CoreLocationManagerProtocol{

    private var locationManager : CLLocationManager!
    private var locations       : [CLLocation] = []
    private var isAvaiable      = false
    
    var whenLocationEnable      : (()->Void)?
    var whenLocationDisable     : (()->Void)?
    var locationUpdateResult    : (([CLLocation]?, Error?) -> Void)?
    
    override init(){
        super.init()
        locationManager = CLLocationManager()
        locationManager.delegate = self
        checkStatus()
    }
    func checkStatus(){
        if CLLocationManager.locationServicesEnabled() {
            let authorizationStatus = CLLocationManager.authorizationStatus()
            switch(authorizationStatus) {
            case .notDetermined, .restricted, .denied:
                locationManager.requestWhenInUseAuthorization()
                isAvaiable = false
                whenLocationDisable?()
            case .authorizedAlways, .authorizedWhenInUse:
                isAvaiable = true
                whenLocationEnable?()
                setupLocationMonitoring()
            }
        } else {
            isAvaiable = false
            whenLocationDisable?()
        }
    }
    private func setupLocationMonitoring(){
        //MARK - Just check how offen need to get update - location accurate
        locationManager.desiredAccuracy = kCLLocationAccuracyHundredMeters
        //locationManager.distanceFilter  = 100.0
        //locationManager.allowsBackgroundLocationUpdates  = true
        locationManager.pausesLocationUpdatesAutomatically = true
        locationManager.activityType = .other
    }
    private func getCurrentLocationAndSleep(){
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestLocation()
    }
    func startLocationMonitor(){
        locationManager.startUpdatingLocation()
    }
    func stopLocationMonitor(){
        locationManager.stopUpdatingLocation()
    }
}
extension CoreLocationManager : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if isAvaiable == false {
            whenLocationEnable?()
            isAvaiable = true
        }
        locationUpdateResult?(locations,nil)
    }
    func locationManager(_ manager: CLLocationManager, didFailWithError error: Error) {
        if let error = error as? CLError, error.code == .denied {
            manager.stopUpdatingLocation()
            isAvaiable = false
            whenLocationDisable?()
        }
        locationUpdateResult?(nil,error)
    }
}
