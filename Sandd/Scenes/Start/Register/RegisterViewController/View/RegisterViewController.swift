//
//  RegisterViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/16/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//
import UIKit
extension Notification.Name {
    static let continueButtonClicked = Notification.Name("completedLengthyDownload")
}
protocol RegisterView : BaseView {
    var onPressBack        : (()->Void)? {get set}
    var onPressSetPassword : (()->Void)? {get set}
}
class RegisterViewController: BaseViewController, RegisterView {
    var onPressBack         : (() -> Void)?
    var onPressSetPassword  : (() -> Void)?

    @IBOutlet weak var textFieldWijkCode: UITextField!
    @IBOutlet weak var textFieldEmail   : UITextField!
    @IBOutlet weak var btnContinue      : UIButton!
    @IBOutlet weak var scrollV          : UIScrollView!
    
    lazy var viewModel : ViewModelRegisterEmailAndWijk = {
        let viewModel = ViewModelRegisterEmailAndWijk(superview:self.view)
        return viewModel
    }()
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    func setupKeyboard(){
        customizeKeyboard(button: btnContinue, textFields: [textFieldWijkCode,textFieldEmail])
        continueButton.addTarget(self, action: #selector(actionContinue(_:)), for: .touchUpInside)
    }
    override func viewWillAppear(_ animated: Bool) {
        self.navigationController?.isNavigationBarHidden = false
        setupKeyboard()
    }
    @IBAction func actionContinue(_ sender: UIButton) {
        activeTextField.resignFirstResponder()
        if EmailAndPasswordCheck().validateEmail(email: textFieldEmail.text!){
            saveUserCredentials()
            onPressSetPassword?()
        }else {
            self.showAllert(title: "Alert", message: "Ongeldige e-mail")
        }
    }
    private func saveUserCredentials(){
        let user = UserCredentials(email: textFieldEmail.text, password: nil, phone: nil, adress: nil, code: textFieldWijkCode.text)
        user.saveUserCredentials()
    }
    @IBAction func actionBack(_ sender: UIBarButtonItem) {
        self.navigationController?.isNavigationBarHidden = true
        onPressBack?()
    }
}
