//
//  ModelLoginViewController.swift
//  Sandd
//
//  Created by Milos Stevanovic on 8/22/18.
//  Copyright © 2018 Branko Grbic. All rights reserved.
//

import Foundation

protocol ModelLoginViewControllerProtocol {
    func getUser()->UserCredentials?
    func saveUser(user : UserCredentials)
}
struct ModelLoginViewController : ModelLoginViewControllerProtocol{

    func getUser() -> UserCredentials? {
            return UserCredentials().getUserCredentials()
    }
    func saveUser(user: UserCredentials) {
        UserCredentials(email: user.email, password: user.password, phone: user.phone, adress: user.adress, code: user.code).saveUserCredentials()
    }
}
